﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Timers;
using DocumentReceiver;

namespace OtusDelegates
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] fileNames = new string[3] { "Паспорт.jpg", "Заявление.txt", "Фото.jpg" };
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string targetDirectory = Path.Combine(desktopPath,"Test1");
            DocumentReceiver.DocumentReceiver receiver =
                new DocumentReceiver.DocumentReceiver(fileNames, targetDirectory, 20000);
            using (receiver)
            {
                receiver.Start();
                receiver.DocumentReady += Receiver_DocumentReady;
                receiver.TimeOut += Receiver_TimeOut;

                Console.WriteLine("Press enter to exit.");
                Console.ReadLine();
            }
        }

        private static void Receiver_TimeOut()
        {
            Console.WriteLine("Врямя закончилось вы не успели загрузить файлы");
        }

        private static void Receiver_DocumentReady(string message)
        {
            Console.WriteLine(message);
        }

    }
}
